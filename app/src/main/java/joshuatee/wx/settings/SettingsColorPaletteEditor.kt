/*

    Copyright 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024  joshua.tee@gmail.com

    This file is part of wX.

    wX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wX.  If not, see <http://www.gnu.org/licenses/>.

 */

package joshuatee.wx.settings

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.widget.EditText
import androidx.appcompat.widget.Toolbar.OnMenuItemClickListener
import joshuatee.wx.R
import joshuatee.wx.common.GlobalVariables
import joshuatee.wx.objects.ObjectDateTime
import joshuatee.wx.radar.NexradUtil
import joshuatee.wx.radarcolorpalettes.ColorPalette
import joshuatee.wx.radarcolorpalettes.UtilityColorPalette
import joshuatee.wx.ui.BaseActivity
import joshuatee.wx.ui.Card
import joshuatee.wx.ui.FabExtended
import joshuatee.wx.ui.ObjectDialogue
import joshuatee.wx.util.To
import joshuatee.wx.util.Utility
import joshuatee.wx.util.UtilityFileManagement
import joshuatee.wx.util.UtilityLog
import joshuatee.wx.util.UtilityShare

class SettingsColorPaletteEditor : BaseActivity(), OnMenuItemClickListener {

    companion object {
        const val URL = ""
    }

    private lateinit var arguments: Array<String>
    private var formattedDate = ""
    private var name = ""
    private var type = ""
    private var typeAsInt = 0
    private lateinit var palTitle: EditText
    private lateinit var palContent: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_settings_color_palette_editor, R.menu.settings_color_palette_editor, true)
        arguments = intent.getStringArrayExtra(URL)!!
        type = arguments[0]
        typeAsInt = type.toIntOrNull() ?: 94
        setTitle("Palette Editor", NexradUtil.productCodeStringToName[typeAsInt]!!)
        setupUI()
        formattedDate = ObjectDateTime.getDateAsString("MMdd")
        name = if (arguments[2].contains("false")) {
            arguments[1]
        } else {
            arguments[1] + "_" + formattedDate
        }
        palTitle.setText(name)
        palContent.setText(UtilityColorPalette.getColorMapStringFromDisk(this, typeAsInt, arguments[1]))
    }

    private fun setupUI() {
        palTitle = findViewById(R.id.palTitle)
        palContent = findViewById(R.id.palContent)
        toolbarBottom.setOnMenuItemClickListener(this)
        FabExtended(this, R.id.fab, GlobalVariables.ICON_DONE, "Save") { savePalette(this) }
        Card(this, R.id.cv1)
        if (UIPreferences.themeInt == R.style.MyCustomTheme_white_NOAB) {
            listOf(palTitle, palContent).forEach {
                it.setTextColor(Color.BLACK)
                it.setHintTextColor(Color.GRAY)
            }
        }
        palTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, UIPreferences.textSizeLarge)
        palContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, UIPreferences.textSizeNormal)
    }

    private fun savePalette(context: Context) {
        val date = ObjectDateTime.getDateAsString("HH:mm")
        val errorCheck = checkMapForErrors()
        if (errorCheck == "") {
            var textToSave = palContent.text.toString()
            textToSave = textToSave.replace(",,".toRegex(), ",")
            palContent.setText(textToSave)
            Utility.writePref(context, "RADAR_COLOR_PAL_" + type + "_" + palTitle.text.toString(), textToSave)
            if (!ColorPalette.radarColorPaletteList[typeAsInt]!!.contains(palTitle.text.toString())) {
                ColorPalette.radarColorPaletteList[typeAsInt] = ColorPalette.radarColorPaletteList[typeAsInt]!! + ":" + palTitle.text.toString()
                Utility.writePref(context, "RADAR_COLOR_PALETTE_" + type + "_LIST", ColorPalette.radarColorPaletteList[typeAsInt]!!)
            }
            toolbar.subtitle = "Last saved: $date"
        } else {
            ObjectDialogue(this, errorCheck)
        }
        val fileName = "colormap" + type + palTitle.text.toString()
        if (UtilityFileManagement.internalFileExist(context, fileName)) {
            UtilityFileManagement.deleteFile(context, fileName)
        }
    }

    private fun checkMapForErrors(): String {
        val text = convertPalette(palContent.text.toString())
        palContent.setText(text)
        val lines = text.split("\n".toRegex()).dropLastWhile { it.isEmpty() }
        var errors = ""
        var priorValue = -200.0
        var lineCount = 0
        lines.forEach { line ->
            if (line.contains("olor") && !line.contains("#")) {
                val list = if (line.contains(",")) {
                    line.split(",")
                } else {
                    line.split(" ")
                }
                lineCount += 1
                try {
                    if (list.size > 4) {
                        if (priorValue >= To.double(list[1])) {
                            errors += "The following lines do not have dbz values in increasing order: " + GlobalVariables.newline + priorValue + " " + list[1] + GlobalVariables.newline
                        }
                        priorValue = To.double(list[1])
                        if (To.double(list[2]) > 255.0 || To.double(list[2]) < 0.0) {
                            errors = errors + "Red value must be between 0 and 255: " + GlobalVariables.newline + line + GlobalVariables.newline
                        }
                        if (To.double(list[3]) > 255.0 || To.double(list[3]) < 0.0) {
                            errors += "Green value must be between 0 and 255: " + GlobalVariables.newline + line + GlobalVariables.newline
                        }
                        if (To.double(list[4]) > 255.0 || To.double(list[4]) < 0.0) {
                            errors += "Blue value must be between 0 and 255: " + GlobalVariables.newline + line + GlobalVariables.newline
                        }
                    } else {
                        errors += "The following line does not have the correct number of command separated entries: " + GlobalVariables.newline + line + GlobalVariables.newline
                    }
                } catch (e: Exception) {
                    errors += "Problem parsing number."
                    UtilityLog.handleException(e)
                }
            }
        }
        if (lineCount < 2) {
            errors += "Not enough lines present."
        }
        return errors
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_reset -> palContent.setText(UtilityColorPalette.getColorMapStringFromDisk(this, typeAsInt, arguments[1]))
            R.id.action_clear -> palContent.setText("")
            R.id.action_share -> UtilityShare.textAsAttachment(this, palTitle.text.toString(), palContent.text.toString(), "wX_colormap_" + palTitle.text.toString() + ".txt")
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onStop() {
        UtilityFileManagement.deleteFile(this, "colormap" + type + palTitle.text.toString())
        super.onStop()
    }

    private fun convertPalette(txt: String): String {
        var txtLocal = txt
                .replace("color", "Color")
                .replace("product", "#product")
                .replace("unit", "#unit")
                .replace("step", "#step")
                .replace(":", " ")
                .trim { it <= ' ' }.replace(" +".toRegex(), " ")
                .trim { it <= ' ' }.replace(" ".toRegex(), ",")
                .replace("\\s".toRegex(), "")
        val lines = txtLocal.split(GlobalVariables.newline.toRegex()).dropLastWhile { it.isEmpty() }
        if (lines.size < 3) {
            txtLocal = txtLocal.replace("Color", GlobalVariables.newline + "Color")
        }
        txtLocal = txtLocal.replace("Step", GlobalVariables.newline + "#Step")
                .replace("Units", GlobalVariables.newline + "#Units")
                .replace("ND", GlobalVariables.newline + "#ND")
                .replace("RF", GlobalVariables.newline + "#RF")
        return txtLocal
    }
}
